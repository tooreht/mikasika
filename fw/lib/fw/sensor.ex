defmodule Fw.Sensor do
  @moduledoc """
  Documentation for Fw.Sensor.
  """
  require Logger

  @doc """
  Read sensor values.

  ## Examples

      iex> Fw.Sensor.read()
      {:ok, 26.6, 21.0, "2019-01-24T19:06:20.533961Z"}

  """
  def read do
    case NervesDHT.device_read(:dht) do
      {:ok, humidity, temperature} ->
        Logger.debug(fn -> "Humidity: #{humidity}, Temperature: #{temperature}" end)
        {:ok, humidity, temperature, DateTime.utc_now()}

      {:error, error} ->
        msg = "Error reading DHT Sensor: #{error}"
        Logger.error(msg)
        {:error, msg}

      _ ->
        msg = "Unknown error"
        Logger.error(msg)
        {:error, msg}
    end
  end
end
