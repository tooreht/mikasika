defmodule Fw.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  @target Mix.Project.config()[:target]

  use Application

  def start(_type, _args) do
    platform_init(@target)
    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Fw.Supervisor]
    Supervisor.start_link(children(@target), opts)
  end

  # List all child processes to be supervised
  def children("host") do
    [
      # Starts a worker by calling: Fw.Worker.start_link(arg)
      # {Fw.Worker, arg},
    ]
  end

  def children(_target) do
    sensor = Application.get_env(:fw, :sensor_type, :dht22)
    pin = Application.get_env(:fw, :sensor_pin, 17)
    webengine_opts = Application.get_all_env(:webengine_kiosk)
    [
      # Sensor
      {NervesDHT, [name: :dht, sensor: sensor, pin: pin]},

      # Configure cron-like job scheduler.
      # See https://github.com/SchedEx/SchedEx for more information.
      # every 10 seconds
      %{ id: "r_dht_every_10s", start: {SchedEx, :run_every, [Fw.Sensor.Publisher, :publish, ["tens"], "*/10 * * * * * *"]} },
      # daily at noon (UTC)
      %{ id: "r_dht_at_noon", start: {SchedEx, :run_every, [Fw.Sensor.Publisher, :publish, ["noon"], "0 12 * * *"]} },

      # Qt WebEngine-based kiosk
      # See https://github.com/fhunleth/webengine_kiosk for more information.
      {WebengineKiosk, {webengine_opts, name: Display}}
    ]
  end

  defp platform_init("host"), do: :ok

  defp platform_init(_target) do
    # Initialize udev :(
    :os.cmd('udevd -d');
    :os.cmd('udevadm trigger --type=subsystems --action=add');
    :os.cmd('udevadm trigger --type=devices --action=add');
    :os.cmd('udevadm settle --timeout=30');
  end
end
