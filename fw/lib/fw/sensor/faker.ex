defmodule Fw.Sensor.Faker do
  @moduledoc """
  Documentation for Fw.Sensor.Faker.
  """

  def random_float(max) do
    (:rand.uniform() * max)
    |> Float.round(2)
  end

  def read do
    # Fake dht sensor values.
    {
      :ok,
      random_float(100),
      random_float(50),
      DateTime.utc_now()
    }
  end
end
