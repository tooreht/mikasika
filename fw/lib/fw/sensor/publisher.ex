defmodule Fw.Sensor.Publisher do
  @moduledoc """
  Documentation for Fw.Sensor.Publisher.
  """
  require Logger
  require Fw.Sensor.Faker

  @doc """
  Publish sensor values.

  ## Examples

      iex> Fw.Sensor.Publisher.publish
      :ok

  """
  def publish(event) do
    {:ok, humidity, temperature, read_at} =
      if Application.get_env(:fw, :fake_sensor, false) do
        Fw.Sensor.Faker.read()
      else
        # Read from real dht sensor connected to the RPi.
        Fw.Sensor.read()
      end

    data = %Ui.Sensor.DhtValue{
      humidity: humidity,
      temperature: temperature,
      read_at: DateTime.to_iso8601(read_at)
    }

    Logger.debug(fn -> "Publish event \"#{event}\" with data #{inspect(data)}" end)

    UiWeb.Endpoint.broadcast!("dht_sensor:values", event, data)
  end
end
