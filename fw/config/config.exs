# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Customize non-Elixir parts of the firmware. See
# https://hexdocs.pm/nerves/advanced-configuration.html for details.

config :nerves, :firmware,
  rootfs_overlay: "rootfs_overlay"
  # fwup_conf: "config/fwup.conf"

# Use shoehorn to start the main application. See the shoehorn
# docs for separating out critical OTP applications such as those
# involved with firmware updates.

config :shoehorn,
  init: [:nerves_runtime, :nerves_init_gadget],
  app: Mix.Project.config()[:app]

# Use Ringlogger as the logger backend and remove :console.
# See https://hexdocs.pm/ring_logger/readme.html for more information on
# configuring ring_logger.

config :logger, backends: [RingLogger]

# Set the number of messages to hold in the circular buffer

config :logger, RingLogger, buffer_size: 100

# Set log level
# See https://www.verypossible.com/blog/thoughtful-logging-in-elixir-a-phoenix-story

config :logger, level: :warn

# Configure nerves_init_gadget.
# See https://hexdocs.pm/nerves_init_gadget/readme.html for more information.

config :nerves_init_gadget,
  ifname: "eth0",
  address_method: :dhcp,
  mdns_domain: "mikasika.local",
  node_name: "mikasika",
  node_host: :mdns_domain

# Wireless networking
# See https://github.com/nerves-project/nerves_network

config :nerves_network,
  regulatory_domain: "US"

key_mgmt = System.get_env("NERVES_NETWORK_KEY_MGMT") || "WPA-PSK"

config :nerves_network, :default,
  wlan0: [
    networks: [
      [
        # WiFi default mode (mode 1), connecting to an access point
        ssid: System.get_env("NERVES_NETWORK_SSID"),
        psk: System.get_env("NERVES_NETWORK_PSK"),
        key_mgmt: String.to_atom(key_mgmt)

        # WiFi host mode (mode 2), creating an access point
        # Note the final `d` in `dhcpd`,
        # this will start `OneDHCPD` on the interface.
        # ssid: "NervesAP",
        # psk: "supersecret",
        # key_mgmt: :"WPA-PSK",
        # mode: 2,
        # ipv4_address_method: :dhcpd
      ]
    ]
  ]

# Configure webengine_kiosk.
# See https://github.com/fhunleth/webengine_kiosk
# and https://github.com/LeToteTeam/kiosk_system_rpi3 for more information.

config :webengine_kiosk,
  fullscreen: true,
  homepage: "http://localhost/", # "file:///var/www/index.html",
  background_color: "black",
  blank_image: "/var/www/assets/nerves.png",
  progress: true,
  sounds: false,
  # HACK: Run Chromium as root user, to work around permission problems.
  # TODO: Investigate why using non-root user fails, maybe related to https://github.com/fhunleth/webengine_kiosk/issues/13?
  run_as_root: true
  # uid: "kiosk",
  # gid: "kiosk",
  # data_dir: "/var/kiosk"

# Authorize the device to receive firmware using your public key.
# See https://hexdocs.pm/nerves_firmware_ssh/readme.html for more information
# on configuring nerves_firmware_ssh.

key = Path.join(System.user_home!(), System.get_env("NERVES_SSH_PUBLIC_KEY") || ".ssh/id_rsa.pub")
unless File.exists?(key), do: Mix.raise("SSH key #{key} not found. Please generate a ssh key")

config :nerves_firmware_ssh,
  authorized_keys: [
    File.read!(key)
]

# Configure nerves_time.
# See https://github.com/fhunleth/nerves_time for more information.

config :nerves_time, :servers, [
  "0.pool.ntp.org",
  "1.pool.ntp.org",
  "2.pool.ntp.org",
  "3.pool.ntp.org"
]

# Configure web server.
# See https://github.com/phoenixframework/phoenix for more information.

config :ui, UiWeb.Endpoint,
  url: [host: "0.0.0.0"],
  http: [port: 80],
  secret_key_base: "AxV43EpSpCX4vgDwbb3Wa4dh5hUDxN+xC7jqqcZcAMx6LpgIc5rnktXi5SPSlmxT",
  root: Path.dirname(__DIR__),
  server: true,
  render_errors: [view: UiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ui.PubSub, adapter: Phoenix.PubSub.PG2],
  code_reloader: false,
  check_origin: false # ["https://example.com", "//another.com:888", "//other.com"]

# Use Jason for JSON parsing in Phoenix

config :phoenix, :json_library, Jason

# Not possible on readonly filesystem
# See https://github.com/bitwalker/timex for more information.

config :tzdata, :autoupdate, :disabled

# Configure firmware

config :fw,
  # Configure DHT sensor type
  sensor_type: :dht22,
  # Configure DHT sensor pin
  sensor_pin: 17,
  # Whether to turn on dht sensor faker, usefull for debugging or when no real dht sensor is available
  fake_sensor: false

# Import target specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
# Uncomment to use target specific configurations

# import_config "#{Mix.Project.config[:target]}.exs"
