defmodule Ui.Sensor.DhtValue do
  @typedoc """
      Type that represents Ui.Sensor.DhtValue struct with
        :humidity as float
        :temperature as float
        :read_at as datetime.
  """
  alias Ui.Sensor.DhtValue

  @derive Jason.Encoder
  defstruct humidity: nil, temperature: nil, read_at: nil

  @type t(humidity, temperature, read_at) :: %DhtValue{
          humidity: humidity,
          temperature: temperature,
          read_at: read_at
        }

  @type t :: %DhtValue{humidity: float, temperature: float, read_at: :utc_datetime}
end
