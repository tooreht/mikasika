defmodule Ui.Sensor.Collector do
  @moduledoc """
  Documentation for Ui.Sensor.Collector.
  """
  use GenServer
  alias Phoenix.Socket.Broadcast
  require Logger

  @doc """
  Collect sensor values.

  ## Examples

      iex> Ui.Sensor.Collector

  """

  def start_link(topic, otp_opts \\ []) when is_binary(topic) do
    GenServer.start_link(__MODULE__, topic, otp_opts)
  end

  def init(topic) do
    UiWeb.Endpoint.subscribe(topic)
    {:ok, []}
  end

  def handle_info(%Broadcast{event: "tens"} = msg, previous_msg) do
    Logger.info(inspect(previous_msg))
    Logger.info(inspect(msg))
    {:noreply, msg}
    # {:noreply, [msg | messages_received]}
  end
end
