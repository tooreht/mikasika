defmodule UiWeb.DhtSensorChannelTest do
  use UiWeb.ChannelCase

  setup do
    {:ok, _, socket} =
      socket(UiWeb.UserSocket, "user_id", %{some: :assign})
      |> subscribe_and_join(UiWeb.DhtSensorChannel, "dht_sensor:values")

    {:ok, socket: socket}
  end

  test "tens replies with status ok", %{socket: socket} do
    dht_sensor_values = %{humidity: 33.333, temperature: 22.22, read_at: DateTime.utc_now()}
    push(socket, "tens", dht_sensor_values)
    assert_broadcast "tens", dht_sensor_values
  end

  test "noon replies with status ok", %{socket: socket} do
    dht_sensor_values = %{humidity: 33.333, temperature: 22.22, read_at: DateTime.utc_now()}
    push(socket, "noon", dht_sensor_values)
    assert_broadcast "noon", dht_sensor_values
  end

  test "ping replies with status ok", %{socket: socket} do
    ref = push(socket, "ping", %{"hello" => "there"})
    assert_reply ref, :ok, %{"hello" => "there"}
  end

  test "shout broadcasts to dht_sensor:values", %{socket: socket} do
    push(socket, "shout", %{"hello" => "all"})
    assert_broadcast "shout", %{"hello" => "all"}
  end

  test "broadcasts are pushed to the client", %{socket: socket} do
    broadcast_from!(socket, "broadcast", %{"some" => "data"})
    assert_push "broadcast", %{"some" => "data"}
  end
end
