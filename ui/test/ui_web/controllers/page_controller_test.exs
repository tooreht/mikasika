defmodule UiWeb.PageControllerTest do
  use UiWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "elm-timelog"
    assert html_response(conn, 200) =~ "elm-sensor"
    assert html_response(conn, 200) =~ "elm-clock"
  end
end
