// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "milligram"
import "../sass/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

import { Elm as ElmClock } from "../elm/src/clock/Clock.elm"
import { Elm as ElmSensor } from "../elm/src/sensor/Sensor.elm"
import { Elm as ElmTimelog } from "../elm/src/timelog/Timelog.elm"

/* ===== *
 * CLOCK *
 * ===== */

const clock = ElmClock.Clock.init({
  node: document.getElementById('elm-clock')
});


/* ====== *
 * SENSOR *
 * ====== */

const phoenix = require('phoenix');
const socketAddress = `${isTruthy(process.env.SENSOR_IS_SECURE) ? 'wss' : 'ws'}://${process.env.SENSOR_HOSTNAME}/socket`;
const websocketPorts = require('elm-phoenix-websocket-ports')(phoenix, socketAddress);

const sensor = ElmSensor.Sensor.init({
  node: document.getElementById('elm-sensor'),
  flags: { // INFO: Flags not in use yet
    isSecure: process.env.SENSOR_IS_SECURE,
    hostName: process.env.SENSOR_HOSTNAME
  }
});

websocketPorts.register(sensor.ports, console.log);


/* ======= *
 * TIMELOG *
 * ======= */

const timelog = ElmTimelog.Timelog.init({
  node: document.getElementById('elm-timelog'),
  flags: { apiEndpoint: process.env.TIMELOG_API_ENDPOINT }
});


/* ===== *
 * UTILS *
 * ===== */

function isTruthy(value) {
  if ((typeof value === 'string' || value instanceof String)) {
    value = value.toLowerCase();
  }
  return value !== 'false' && value !== 'no' && !!value;
}
