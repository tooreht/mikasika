module Clock exposing (..)

import Browser
import Html exposing (..)
import Svg exposing (circle, line, svg)
import Svg.Attributes exposing (..)
import Task
import Time exposing (Posix, Zone, toHour, toMinute, toSecond)


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type Model
    = Loading
    | GotZone Zone
    | WithTime CurrentTime


type alias CurrentTime =
    { zone : Zone
    , time : Posix
    }


type Msg
    = GetZone Zone
    | Tick Posix
    | NoOp Posix


-- UTILITIES

printWithLeadingZero : Int -> String
printWithLeadingZero number =
    if number < 10 then
        "0" ++ (String.fromInt number)
    else
        String.fromInt number

degreesCorrection : Float
degreesCorrection =
    90.0 -- The correction we must do on our analog clock to show 12 pointing up instead to the right

degreesForHour : Float
degreesForHour =
    360.0 / 12.0 -- We divide the total degrees of a full circle by the full hours of the day to get the degrees per hour

degreesForMinute : Float
degreesForMinute =
    360.0 / 60.0 -- We divide the total degrees of a full circle by the full minutes of the hour to get the degrees per minute

degreesForSecond : Float
degreesForSecond =
    360.0 / 60.0 -- We divide the total degrees of a full circle by the full seconds of the minute to get the degrees per second

convertToDegrees : Int -> Float -> Float
convertToDegrees value degreesPerPoint =
    degrees (((toFloat value) * degreesPerPoint) - degreesCorrection)

secondsToDegrees : Int -> Float
secondsToDegrees seconds =
    convertToDegrees seconds degreesForSecond

minutesToDegrees : Int -> Float
minutesToDegrees minutes =
    convertToDegrees minutes degreesForMinute

hoursToDegrees : Int -> Float
hoursToDegrees hours =
    convertToDegrees hours degreesForHour


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading, Task.perform GetZone Time.here )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp _ ->
            ( model, Cmd.none )

        GetZone zone ->
            case model of
                Loading ->
                    ( GotZone zone, Cmd.none )

                GotZone _ ->
                    ( GotZone zone, Cmd.none )

                WithTime ct ->
                    ( WithTime { ct | zone = zone }
                    , Cmd.none
                    )

        Tick time ->
            case model of
                Loading ->
                    ( model, Cmd.none )

                GotZone zone ->
                    ( WithTime { zone = zone, time = time }
                    , Cmd.none
                    )

                WithTime ct ->
                    ( WithTime { ct | time = time }
                    , Cmd.none
                    )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading ->
            -- Sub.none
            Time.every 1 NoOp

        _ ->
            Time.every 1000 Tick


view : Model -> Html Msg
view model =
    case model of
        WithTime ct ->
            -- viewTime ct
            viewAnalogTime ct

        _ ->
            h1 [] [ text "Loading..." ]


viewTime : CurrentTime -> Html Msg
viewTime { zone, time } =
    List.map (\f -> f zone time) [ toHour, toMinute, toSecond ]
        |> List.map String.fromInt
        |> String.join " : "
        |> (\t -> h1 [] [ text t ])

viewAnalogTime : CurrentTime -> Html Msg
viewAnalogTime { zone, time } =
    let
        second =
            toSecond zone time

        secondAngle =
            secondsToDegrees second

        secondHandX =
            String.fromFloat (50 + 40 * cos secondAngle)

        secondHandY =
            String.fromFloat (50 + 40 * sin secondAngle)

        minute =
            toMinute zone time

        minuteAngle =
            minutesToDegrees minute

        minuteHandX =
            String.fromFloat (50 + 35 * cos minuteAngle)

        minuteHandY =
            String.fromFloat (50 + 35 * sin minuteAngle)

        hour =
            toHour zone time

        hourAngle =
            hoursToDegrees hour

        hourHandX =
            String.fromFloat (50 + 30 * cos hourAngle)

        hourHandY =
            String.fromFloat (50 + 30 * sin hourAngle)

        currentTime =
            (printWithLeadingZero hour) ++ ":" ++ (printWithLeadingZero minute) ++ ":" ++ (printWithLeadingZero second)

    in
    div [ class "container clock" ]
        [ div [ class "digital" ] [ text currentTime ]
        , svg [ viewBox "0 0 100 100", width "200px" ]
            [ circle [ cx "50", cy "50", r "45", fill "#0B79CE" ] []
            , line [ x1 "50", y1 "50", x2 secondHandX, y2 secondHandY, stroke "#9b4dca" ] []
            , line [ x1 "50", y1 "50", x2 minuteHandX, y2 minuteHandY, stroke "#0000ee" ] []
            , line [ x1 "50", y1 "50", x2 hourHandX, y2 hourHandY, stroke "#023963" ] []
            ]
        ]
