module Update exposing (update)

--

import Actions exposing (..)
import Constants
import Model exposing (..)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of

        WebsocketReceive ("dht_sensor:values", "tens", payload) ->
            Actions.websocketReceive model payload

        WebsocketReceive ( _, _, _ ) ->
            ( model, Cmd.none )

        WebsocketSend ("dht_sensor:values", "tens", payload) ->
            ( model
            , Actions.websocketSend model
            )

        WebsocketSend ( _, _, _ ) ->
            ( model, Cmd.none )

