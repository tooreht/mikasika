module Actions exposing (websocketSend, websocketReceive)

-- import Debug

import Constants
import DhtValue exposing (dhtValueDecoder)
import Json.Decode as D
import Json.Encode as E
import Model exposing (..)
import Ports.Websocket
import Time


websocketSend : Model -> Cmd Msg
websocketSend model =
    Ports.Websocket.websocketSend
        ( Constants.phoenixTopic
        , Constants.sensorUpdateEvent
        , E.object [ ("humidity", E.float model.dhtValue.humidity), ("temperature", E.float model.dhtValue.temperature)]
        )


websocketReceive : Model -> E.Value -> ( Model, Cmd Msg )
websocketReceive model payload =
    case D.decodeValue dhtValueDecoder payload of
        Ok dhtValue ->
            -- Debug.log (String.fromFloat dhtValue.humidity)
            -- Debug.log (String.fromFloat dhtValue.temperature)
            -- Debug.log dhtValue.read_at
            ( { model | dhtValue = dhtValue }
            , Cmd.none
            )

        Err error ->
            ( model, Cmd.none )
