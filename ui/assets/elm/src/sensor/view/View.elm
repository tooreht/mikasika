module View exposing (view)

import DateFormat

import Html exposing (Html, div, h2, h5, text)
import Html.Attributes exposing (class, id)
import Model exposing (..)
import Time exposing (Posix, Zone, utc)


view : Model -> Html Msg
view model =
    div [ class "container sensor" ]
        [ h2 [] [ text "Sensor" ]
        , div [ class "row" ]
          [ div [ class "column" ]
            [ div [ id "read-at", class "values" ] [ text (format utc model.dhtValue.read_at) ]
            ]
          ]
        , div [ class "row" ]
            [ div [ class "column" ]
                [ div [] [ h5 [] [ text "Humidity" ] ]
                , div [ id "humidity", class "values" ] [ text (String.fromFloat model.dhtValue.humidity ++ "%") ]
                ]
            , div [ class "column" ]
                [ div [] [ h5 [] [ text "Temperature" ] ]
                , div [ id "temperature", class "values" ] [ text (String.fromFloat model.dhtValue.temperature ++ "°C") ]
                ]
            ]
        ]


format : Zone -> Posix -> String
format =
    DateFormat.format
        [ DateFormat.hourFixed
        , DateFormat.text ":"
        , DateFormat.minuteFixed
        , DateFormat.text ":"
        , DateFormat.secondFixed
        , DateFormat.text " "
        , DateFormat.monthNameFull
        , DateFormat.text " "
        , DateFormat.dayOfMonthSuffix
        , DateFormat.text ", "
        , DateFormat.yearNumber
        ]