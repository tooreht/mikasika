module Subscriptions exposing (subscriptions)

--

import Model exposing (..)
import Ports.Websocket


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Ports.Websocket.websocketReceive WebsocketReceive
        ]
