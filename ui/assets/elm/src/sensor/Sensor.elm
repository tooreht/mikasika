module Sensor exposing (main)

--

import Browser
import Constants
import Flags exposing (Flags, decoder, initial)
import Json.Decode as D
import Json.Encode exposing (Value)
import Model exposing (..)
import Ports.Websocket
import Subscriptions
import Update
import View


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , view = View.view
        , update = Update.update
        , subscriptions = Subscriptions.subscriptions
        }

init : Value -> ( Model, Cmd Msg )
init flags =
    ( flags
        |> D.decodeValue decoder
        |> Result.withDefault initial
        |> Model.init
    , Ports.Websocket.websocketListen (Constants.phoenixTopic, Constants.sensorUpdateEvent)
    )
