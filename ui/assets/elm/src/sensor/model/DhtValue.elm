module DhtValue exposing (DhtValue, dhtValueDecoder)

import Json.Decode as D exposing (andThen, field, string)
import Json.Decode.Extra exposing (fromResult)
import Time exposing (..)
import Iso8601


type alias DhtValue =
    { humidity : Float
    , temperature : Float
    , read_at : Time.Posix
    }


dhtValueDecoder : D.Decoder DhtValue
dhtValueDecoder =
    D.map3 DhtValue
        (field "humidity" D.float)
        (field "temperature" D.float)
        (field "read_at" Iso8601.decoder)
