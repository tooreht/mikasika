module Model exposing (Model, Msg(..), init)

-- import Phoenix.Channel
-- import Phoenix.Push
--

import DhtValue exposing (DhtValue)
import Flags exposing (..)
import Json.Encode as E
import Json.Decode as D
import Time exposing (..)


type alias Model =
    { dhtValue : DhtValue
    , flags: Flags
    }


type Msg
    = WebsocketReceive (String, String, E.Value)
    | WebsocketSend (String, String, D.Value)


init : Flags -> Model
init flags =
    { dhtValue = DhtValue 0 0 (Time.millisToPosix 0)
    , flags = flags
    }
