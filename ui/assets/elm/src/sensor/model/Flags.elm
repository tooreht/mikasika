module Flags exposing (Flags, decoder, initial)

import Json.Decode as D


type alias Flags =
    { isSecure : Bool
    , hostName : String
    }


decoder: D.Decoder Flags
decoder =
    D.map2 Flags
        (D.field "isSecure" D.bool)
        (D.field "hostName" D.string)


initial: Flags
initial =
    { isSecure = False
    , hostName = "localhost:4000"
    }