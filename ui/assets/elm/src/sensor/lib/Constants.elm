module Constants exposing
    ( phoenixTopic
    , sensorUpdateEvent
    , urlBase
    , wsBase
    )

import Flags exposing (..)


{- Currently not in use
   Until the elm/websockets packages is overhauled for 0.19, ports are being used.
   See - https://discourse.elm-lang.org/t/updating-packages/1771
       - https://guide.elm-lang.org/interop/ports.html
-}
urlBase : Flags -> String
urlBase flags =
    if flags.isSecure then
        "https://" ++ flags.hostName

    else
        "http://" ++ flags.hostName


{- Currently not in use
   Until the elm/websockets packages is overhauled for 0.19, ports are being used.
   See - https://discourse.elm-lang.org/t/updating-packages/1771
       - https://guide.elm-lang.org/interop/ports.html
-}
wsBase : Flags -> String
wsBase flags =
    if flags.isSecure then
        "wss://" ++ flags.hostName

    else
        "ws://" ++ flags.hostName


phoenixTopic : String
phoenixTopic =
    "dht_sensor:values"


sensorUpdateEvent : String
sensorUpdateEvent =
    "tens"
