module Timelog exposing (Model, Msg(..), addToast, decodeContent, init, main, myConfig, sendPin, update, view)

import Array exposing (Array)
import Browser
import Html exposing (Html, button, div, h2, input, span, text)
import Html.Attributes exposing (attribute, class, src, type_, value)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as D exposing (Decoder, at)
import Json.Encode exposing (Value)
import Toasty
import Toasty.Defaults



---- MODEL ----


type alias Model =
    { pin : Array Int
    , toasties : Toasty.Stack Toasty.Defaults.Toast
    , flags: Flags
    }

type alias Flags =
    { apiEndpoint : String }

flagsDecoder : D.Decoder Flags
flagsDecoder =
    D.map Flags
        (D.field "apiEndpoint" D.string)

init : Value -> ( Model, Cmd Msg )
init value =
    let
        flags = value
            |> D.decodeValue flagsDecoder
            |> Result.withDefault initial
    in
    ( Model Array.empty Toasty.initialState flags
    , Cmd.none
    )

initial: Flags
initial =
    { apiEndpoint = "http://localhost:4000" }


myConfig : Toasty.Config Msg
myConfig =
    Toasty.Defaults.config
        |> Toasty.delay 5000


addToast : Toasty.Defaults.Toast -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
addToast toast ( model, cmd ) =
    Toasty.addToast myConfig ToastyMsg toast ( model, cmd )



---- UPDATE ----


type Msg
    = AddDigit Int
    | RemoveDigit
    | SendPin
    | ReceivePinAck (Result Http.Error String)
    | ToastyMsg (Toasty.Msg Toasty.Defaults.Toast)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddDigit digit ->
            ( { model | pin = Array.push digit model.pin }, Cmd.none )

        RemoveDigit ->
            ( { model | pin = Array.slice 0 -1 model.pin }, Cmd.none )

        SendPin ->
            let
                request =
                    sendPin model
            in
            ( { model | pin = Array.empty }, Http.send ReceivePinAck request )

        ReceivePinAck result ->
            case result of
                Err httpError ->
                    (( model
                     , Cmd.none
                     )
                    )
                        |> addToast (Toasty.Defaults.Error "Error" "PIN not sent")

                Ok message ->
                    (( model
                     , Cmd.none
                     )
                    )
                        |> addToast (Toasty.Defaults.Success "Success" message)

        ToastyMsg subMsg ->
            Toasty.update myConfig ToastyMsg subMsg model


decodeContent : Decoder String
decodeContent =
    at [ "data", "msg" ] D.string


sendPin : Model -> Http.Request String
sendPin model =
    Http.get (model.flags.apiEndpoint ++ String.concat (Array.toList (Array.map String.fromInt model.pin))) decodeContent



---- VIEW ----


view : Model -> Html Msg
view model =
    div [ class "container timelog" ]
    [
        h2 [] [ text "Timelog" ]
    ,
        div [ class "container pin-wrap" ]
        [
            div [ class "row" ]
                [ div [ class "column display" ]
                    [ input [ type_ "password", 
                              attribute "inputmode" "numeric",
                              value (String.join "" <| Array.toList <| Array.map String.fromInt model.pin),
                              class "pin-code"
                            ] [] ]
                ]
        ,
            div [ class "row" ]
                [ div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 1) ] [ text "1" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 2) ] [ text "2" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 3) ] [ text "3" ] ]
                ]
        ,
            div [ class "row" ]
                [ div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 4) ] [ text "4" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 5) ] [ text "5" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 6) ] [ text "6" ] ]
                ]
        ,
            div [ class "row" ]
                [ div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 7) ] [ text "7" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 8) ] [ text "8" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 9) ] [ text "9" ] ]
                ]
        ,
            div [ class "row" ]
                [ div [ class "column pin" ] [ button [ class "button", onClick RemoveDigit ] [ text "✗" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick (AddDigit 0) ] [ text "0" ] ]
                , div [ class "column pin" ] [ button [ class "button", onClick SendPin ] [ text "✔" ] ]
                ]
        ]
    , Toasty.view myConfig Toasty.Defaults.view ToastyMsg model.toasties
    ]



---- PROGRAM ----


main : Program Value Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        }
