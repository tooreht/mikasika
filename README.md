# Mikasika

Mikasika is the [verb *to touch* in malagasy](http://malagasyword.org/bins/teny2?w=mikasika). The purpose of this project is a firmware for the [Raspberry Pi 3](https://www.raspberrypi.org/) with a [7" Touchscreen Display](https://www.raspberrypi.org/products/raspberry-pi-touch-display/) with the following features:

- Read from a [DHT](https://learn.adafruit.com/dht) sensor
- Display humidity and temperature on the touchscreen
- Allow employes to log their work time on the touchscreen

## Overview

Built with [elixir](https://elixir-lang.org/) using:

- [nerves](https://nerves-project.org/) for firmware
- [phoenix](https://phoenixframework.org/) as web framework
- [elm](https://elm-lang.org/) for ui components

```txt
.
├── CONTRIBUTING.txt
├── CONTRIBUTORS.txt
├── README.md
├── fw                          firmware using nerves
│   ├── README.md
│   ├── _build
│   ├── config
│   ├── deps
│   ├── fw.img
│   ├── lib
│   ├── mix.exs
│   ├── mix.lock.host
│   ├── mix.lock.rpi3
│   ├── rel
│   ├── rootfs_overlay
│   ├── test
│   └── upload.sh
└── ui                          user interface using phoenix and elm
    ├── README.md
    ├── _build
    ├── assets                  elm, nodejs, sass, static files, etc.
    ├── config
    ├── deps
    ├── lib
    ├── mix.exs
    ├── mix.lock
    ├── priv
    └── test
```

## Installation

1. Get source code

    ```bash
    git clone ...
    ```

2. Enter the project

    ```bash
    cd mikasika
    ```

### Firmware

1. Enter fw

    ```bash
    cd fw
    ```

2. Install dependencies

    ```bash
    MIX_TARGET=rpi3 mix deps.get
    ```

3. Create firmware

    ```bash
    MIX_TARGET=rpi3 mix firmware
    ```

4. Burn to an SD card

    ```bash
    MIX_TARGET=rpi3 mix firmware.burn
    ```

### User Interface

1. Enter ui

    ```bash
    cd ui
    ```

2. Install dependencies

    ```bash
    mix deps.get
    ```

3. Install [yarn](https://yarnpkg.com/lang/en/docs/install/)

4. Install Node.js dependencies

    ```bash
    cd assets && yarn
    ```

5. Start Phoenix endpoint

    ```bash
    mix phx.server
    ```

## Configuration

Have a look at the following files:

- `fw/config/*`
- `ui/config/*`
- `ui/assets/.env`
- `ui/assets/package.json`
- `ui/assets/js/app.js`
- `ui/assets/elm/elm.json`

### Nerves

Basic usage

```bash
MIX_TARGET=rpi3 mix firmware
```

Using a different SSH public key than `~/.ssh/id_rsa.pub`

```bash
MIX_TARGET=rpi3 NERVES_SSH_PUBLIC_KEY=.ssh/id_ed25519.pub mix firmware
```

Using WLAN

```bash
MIX_TARGET=rpi3 NERVES_NETWORK_SSID="my_ssid" NERVES_NETWORK_PSK="my_secret_psk" mix firmware
```

### DHT Sensor

The system is configured to read dht sensor values from GPIO pin 17.

### Elm

Create a `.env` file in the `./ui/assets/` directory with the following environment variables:

```bash
SENSOR_IS_SECURE=false                                  // whether the connection is secure (http and ws)
SENSOR_HOSTNAME=10.0.0.108                              // hostname or IP of the RPi
TIMELOG_API_ENDPOINT='http://localhost/api/timelogs/'   // URL of the timelog API endpoint
```

## Usage

1. Edit code and run

    ```bash
    mix format
    ```

2. Compile assets

    ```bash
    cd ui/assets
    npm run deploy
    cd ..
    ```

3. Digest and compress static files

    ```bash
    mix phx.digest
    ```

4. Build firmware

    ```bash
    cd ../fw
    MIX_ENV=prod MIX_TARGET=rpi3 mix firmware
    ```

5. Deploy to the RPi

    To bootstrap a RPi installation, the firmware needs to be put on the SD card via:

    ```bash
    MIX_ENV=prod MIX_TARGET=rpi3 mix firmware.burn
    ```

    Once the RPi is up and running, the firmware can be upgraded over the air/network via ssh.

    If `upload.sh` is not already generated:

    ```bash
    MIX_TARGET=rpi3 mix firmware.gen.script
    ```

    Initiate upload with:

    ```bash
    MIX_ENV=prod ./upload.sh [destination IP] [Path to .fw file]
    ```

## Development

### Ui

The UI is optimized for the [RPi Touch Display](https://www.raspberrypi.org/products/raspberry-pi-touch-display/) with a screen resolution of 800 x 480 pixels.

TODO: Add more documentation about ui backend and frontend code.

### Debug on the RPi over SSH

```bash
$ ssh <rpi_ip>
Interactive Elixir (1.7.4) - press Ctrl+C to exit (type h() ENTER for help)
Toolshed imported. Run h(Toolshed) for more info
RingLogger is collecting log messages from Elixir and Linux. To see the
messages, either attach the current IEx session to the logger:

  RingLogger.attach

or print the next messages in the log:

  RingLogger.next

iex(mikasika@mikasika.local)1>
iex(mikasika@mikasika.local)2> h(Toolshed)

                                    Toolshed

Making the IEx console friendlier one command at a time

To use the helpers, run:

    iex> use Toolshed

Add this to your .iex.exs to load automatically.

The following is a list of helpers:

  • cmd/1          - run a system command and print the output
  • hex/1          - print a number as hex
  • top/2          - list out the top processes
  • exit/0         - exit out of an IEx session
  • cat/1          - print out a file
  • grep/2         - print out lines that match a regular expression
  • tree/1         - pretty print a directory tree
  • hostname/0     - print our hostname
  • nslookup/1     - query DNS to find an IP address
  • tping/1        - check if a host can be reached (like ping, but uses
    TCP)
  • ifconfig/0     - print info on network interfaces
  • dmesg/0        - print kernel messages (Nerves-only)
  • reboot/0       - reboots gracefully (Nerves-only)
  • reboot!/0      - reboots immediately  (Nerves-only)
  • fw_validate/0  - marks the current image as valid (check Nerves system
    if supported)
  • save_value/2   - save a value to a file as Elixir terms (uses inspect)
  • save_term!/2   - save a term as a binary
  • load_term!/2   - load a term that was saved by save_term/2
  • lsusb/0        - print info on USB devices
  • uptime/0       - print out the current Erlang VM uptime

iex(mikasika@mikasika.local)3>
```

Lower the log level at runtime

```bash
iex(mikasika@mikasika.local)3> Logger.configure(level: :debug)
:ok

21:51:40.005 [debug] Publish event "tens" with data %Ui.Sensor.DhtValue{humidity: 22.9, read_at: "2019-01-19T21:51:40.004573Z", temperature: 47.03}
```

### Emulate Sensor Update when running fw

This is helpful if no real dht sensor can be connected to the RPi.

Set the `fake_dht_sensor` flag in `fw/config/config.exs` accordingly:

```elixir
# Whether to turn on dht sensor faker, usefull for debugging or when no real dht sensor is available

config :fw, fake_dht_sensor: true
```

Turn on dht sensor faker at runtime

```bash
$ ssh <rpi_ip>
Interactive Elixir (1.7.4) - press Ctrl+C to exit (type h() ENTER for help)
Toolshed imported. Run h(Toolshed) for more info
RingLogger is collecting log messages from Elixir and Linux. To see the
messages, either attach the current IEx session to the logger:

  RingLogger.attach

or print the next messages in the log:

  RingLogger.next

iex(mikasika@mikasika.local)1> Logger.configure(level: :debug)
iex(mikasika@mikasika.local)2> Application.put_env(:fw, :fake_dht_sensor, true)
:ok

21:51:42.004 [debug] Publish event "tens" with data %Ui.Sensor.DhtValue{humidity: 53.0, read_at: "2019-01-19T21:51:42.003898Z", temperature: 23.65}
```

### Emulate Sensor Update when running ui only

Start web server in interactive prompt and emit fake sensor read event:

```bash
$ cd ui
$ iex -S mix phx.server
Erlang/OTP 21 [erts-10.2] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1]

[info] Running UiWeb.Endpoint with cowboy 2.6.1 at http://localhost:4000
Interactive Elixir (1.7.4) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)>
webpack is watching the files…
...
iex(2)> UiWeb.Endpoint.broadcast! "dht_sensor:values", "tens", %{humidity: 33.333, temperature: 22.22, read_at: DateTime.utc_now}
:ok
```